# Movie-hero

## Css and Javascript Animation

Simple app where you press the card and, it show up the caracthere behind and also his/her voice.When pressing it again, it goes back to the initial card.

First project where I start using Javascript. 
Even that the code is ugly and there is big room for improvement I want to leave it like that, to remind me always from where I start. Also because this project was not based in any tutorial I want to leave it the way it is.


In this project I pratice my skills in `Dom Manipulation` and also `CSS Animations`.



In `Javascript` learn how to interact with the HTML and also get to know more about functions

In `CSS`, I use  `CSS` Grid and some animation for the images and texts.

Resuming:
<p>Skills Learned:</p>

<ul>
    <li>Dom Manipulation</li>
    <li>CSS Grid</li>
    <li>Functions</li>
    <li>For Loops</li>
    <li>CSS Animations</li>
</ul>
